# React translation app which translates English into American sign language.

# Install instructions
Use npm -i in the project root folder. 

Backend is managed by json-server. Use json-server --watch db.json

Image assets are hosted on Google Cloud Storage.

Login and logout is managed by auth0. Make sure not to change the default port of the react server (set up to 4040).

